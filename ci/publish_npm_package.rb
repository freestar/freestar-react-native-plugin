require 'json'

PACKAGE_NAME = 'freestar-plugin-react'
PACKAGE_DIR = 'plugin'

puts "#{%x(npm whoami)}"

new_json = JSON.parse(File.read("#{PACKAGE_DIR}/package.json"))
current_json = JSON.parse(%x(npm info #{PACKAGE_NAME} --json))

if new_json["version"] == current_json["version"] # no change
    puts "Nothing to publish."
    exit
end

new_version = new_json["version"].split(".").map {|x| x.to_i }
current_version = current_json["version"].split(".").map {|x| x.to_i }


if new_version[0] > current_version[0]
    puts "Publishing major version update."
elsif new_version[0] == current_version[0]
    if new_version[1] > current_version[1]
        puts "Publishing minor version update."
    elsif new_version[1] == current_version[1]
        if new_version[2] > current_version[2]
            puts "Publishing bug update."
        elsif new_version[2] == current_version[2]
            puts "No version update detected. Nothing to publish."
            exit
        else
            raise "Attempt to downgrade bug version. Not allowed."
        end
    else
        raise "Attempt to downgrade minor version. Not allowed."
    end
else
    raise "Attempt to downgrade major version. Not allowed."
end

system("cd plugin && npm publish")